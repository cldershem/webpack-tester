import React, { Component } from 'react';

import Header from './Header';
import Footer from './Footer';
import Hero from './Hero';


export default class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Hero />
        <Footer />
      </div>
    )
  }
}
