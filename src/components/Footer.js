import React, { Component } from 'react';

class FooterLink extends Component {
  render() {
    return (
      <li><a href={this.props.url}>{this.props.label}</a></li>
    )
  }
}


export default class Footer extends Component {
  render() {
    const items = [
      { label: 'about', url: '#' },
      { label: 'contact us', url: '#' },
      { label: 'terms & conditions', url: '#' },
      { label: 'privacy policy', url: '#' },
    ];

    return (
      <footer>
        <ul>
          {items.map(item =>
            <FooterLink key={item.label} url={item.url} label={item.label} />
          )}
          <li>COPYRIGHT 2017 TRAINERPUSH INC</li>
        </ul>
      </footer>
    );
  }
}
