import React, { Component } from 'react';

import HeroBG from '../assets/img/hero-background.jpeg';

class HeroLink extends Component {
  render() {
    return (
      <a className="hero-link" href={this.props.url}>{this.props.label}</a>
    )
  }
}


export default class Footer extends Component {
  render() {
    const items = [
      { label: 'how it works', url: '#' },
      { label: 'get stated right', url: '#' },
    ];

    return (
      <div id="hero">
        <div id="hero-center">
          {items.map(item =>
            <HeroLink key={item.label} url={item.url} label={item.label} />
          )}
        </div>
      </div>
    );
  }
}
