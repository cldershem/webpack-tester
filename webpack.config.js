const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const DIST_DIR = path.resolve(__dirname, 'dist');

const jsRules = {
  test: /.js$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'babel-loader',
      options: {
        cacheDirectory: true,
      },
    },
  ],
};

const cssRules = {
  test: /\.css$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader',
  }),
};

const imgRules = {
  test: /\.(png|svg|jpg|jpeg|gif)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: 'static/img/[hash].[ext]',
      },
    },
  ],
};

module.exports = (env) => {
  return {
    entry: './src/index.js',
    output: {
      filename: 'bundle.js',
      path: DIST_DIR,
      publicPath: '/dist/',
    },
    resolve: {
      alias: {
        img_dir: path.resolve(__dirname, 'src/assets/img'),
      }
    },
    module: {
      rules: [
        jsRules,
        imgRules,
        cssRules,
      ],
    },
    devtool: 'source-map',
    plugins: [
      new ExtractTextPlugin({
        filename: 'static/css/styles.css',
      }),
    ],
  };
};
