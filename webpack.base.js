const path = require('path');
const webpack = require('webpack');

const jsRules = {
  test: /.js$/,
  exclude: /node_modules/,
  use: [
    {
      loader: 'babel-loader',
      options: {
        cacheDirectory: true,
      },
    },
  ],
};

module.exports = {
  entry: './src/index.js',
  module: {
    rules: [
      jsRules,
    ],
  },
  plugins: [
    new webpack.EnvironmentPlugin([
      'NODE_ENV',
    ]),
  ],
};
