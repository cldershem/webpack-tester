const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');
const DIST_DIR = path.resolve(__dirname, 'dist');

const cssRules = {
  test: /\.css$/,
  use: ExtractTextPlugin.extract({
    fallback: 'style-loader',
    use: 'css-loader',
  }),
};

const imgRules = {
  test: /\.(png|svg|jpg|jpeg|gif)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: 'static/img/[hash].[ext]',
      },
    },
  ],
};

module.exports = merge(baseConfig, {
  output: {
    filename: 'bundle.js',
    path: DIST_DIR,
    publicPath: '/dist/',
  },
  module: {
    rules: [
      imgRules,
      cssRules,
    ],
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'static/css/styles.css',
    }),
  ],
});
