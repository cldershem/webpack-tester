# FakeCompany-Frontend
## [DEMO](https://cldershem.gitlab.io/webpack-tester)
## Install
```
git@gitlab.com:cldershem/webpack-tester.git
cd webpack-tester
npm install
```

## Run
```
npm start
```

## Build
```
npm run build
```
