const merge = require('webpack-merge');
const baseConfig = require('./webpack.base');

const cssRules = {
  test: /\.css$/,
  use: [
    'style-loader',
    'css-loader',
  ],
};

const imgRules = {
  test: /\.(png|svg|jpg|jpeg|gif)$/,
  use: [
    {
      loader: 'file-loader',
      options: {
        name: 'static/img/[hash].[ext]',
      },
    },
  ],
};

module.exports = merge(baseConfig, {
  module: {
    rules: [
      imgRules,
      cssRules,
    ],
  },
  devtool: 'source-map',
  // devServer: {
  //   publicPath: './dist/',
  // },
});
